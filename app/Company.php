<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function type() {
        return $this->hasOne('App\CompanyType', 'id','type_id');
    }
}
