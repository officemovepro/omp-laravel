<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Address;
use App\City;
use App\CompanyType;
use App\CompanyPerson;
use App\Company;
use App\Country;
use App\Person;
use App\Project;
use App\ProjectService;
use App\ProjectServicesNotes;
use App\Province;
use App\Service;

class DataController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
    ini_set('max_execution_time', 1500); // 300 seconds = 5 minutes
    set_time_limit(0);
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {
    $company_people = CompanyPerson::orderBy('status')->get();
    // dd($companies);
    return view('data',['company_people' => $company_people]);
  }

  public function create_company_types() {
    $types = ['Customer','Branch'];
    foreach($types as $type) {
      $company_type = new CompanyType;
      $company_type->name = $type;
      $company_type->save();
    }
    return redirect(route('data'));
  }

  public function import_companies() {
    $old_companies = DB::connection('mysql2')->select('SELECT * FROM Companies');
    foreach($old_companies as $old) {
      $company = new Company;
      $company->old_id = $old->id;
      $company->id = $old->id;
      $company->type_id = $old->type_id;
      $company->name = $old->name;
      $company->email = $old->email;
      $company->url = $old->url;
      $company->phone = $old->phone;
      $company->fax = $old->fax;
      $company->note = $old->note;
      $company->parent_company_id = $old->parent_company_id;
      $company->primary_colour = $old->primary_colour;
      $company->secondary_colour = $old->secondary_colour;
      $company->logo_url = $old->logo_url;
      $company->url_prefix = $old->url_prefix;
      $company->verified_on = $old->verified_on;
      $company->save();
    }
    return redirect(route('data'));
  }

  public function import_cities() {
    $old_cities = DB::connection('mysql2')->select('SELECT * FROM Cities');
    foreach($old_cities as $old) {
      $city = new City;
      $city->old_id = $old->id;
      $city->id = $old->id;
      $city->name = $old->name;
      $city->province_id = $old->province_id;
      $city->omn_tenant_id = $old->omn_tenant_id;
      $city->save();
    }
    return redirect(route('data'));
  }

  public function import_countries() {
    $old_countries = ['Canada', 'United States'];
    foreach($old_countries as $old) {
      $country = new Country;
      $country->name = $old;
      $country->save();
    }
    return redirect(route('data'));
  }

  public function import_provinces() {
    $old_provinces = DB::connection('mysql2')->select('SELECT * FROM Provinces');
    foreach($old_provinces as $old) {
      $province = new Province;
      $province->id = $old->id;
      $province->old_id = $old->id;
      $province->country_id = $old->country_id;
      $province->abbreviation = $old->abbreviation;
      $province->name = $old->name;
      $province->save();
    }
    return redirect(route('data'));
  }

  public function import_addresses() {
    $old_addresses = DB::connection('mysql2')->select('SELECT * FROM Addresses');
    foreach($old_addresses as $old) {
      $address = new Address;
      $address->old_id = $old->id;
      $address->id = $old->id;
      $address->building_name = $old->building_name;
      $address->description = $old->description;
      $address->unit_designator = $old->unit_designator;
      $address->unit = $old->unit;
      $address->floor = $old->floor;
      $address->civic_number = $old->civic_number;
      $address->civic_number_suffix = $old->civic_number_suffix;
      $address->street_name = $old->street_name;
      $address->address_street_type_id = $old->address_street_type_id;
      $address->street_direction = $old->street_direction;
      $address->rural_route = $old->rural_route;
      $address->station = $old->station;
      $address->city_id = $old->city_id;
      $address->postal_code = $old->postal_code;
      $address->notes = $old->notes;
      $address->latitude = $old->latitude;
      $address->longitude = $old->longitude;
      $address->omn_tenant_id = $old->omn_tenant_id;
      $address->save();
    }
    return redirect(route('data'));
  }

  public function import_people() {
    $old_people = DB::connection('mysql2')->select('SELECT * FROM People');
    // dd($old_people);
    $titles = [
      1 => 'Mr.',
      2 => 'Mrs.',
      4 => 'Ms.',
      5 => 'Miss'
    ];
    foreach($old_people as $old) {
      $person = new Person;
      $person->old_id = $old->id;
      $person->id = $old->id;
      $person->first_name = $old->first_name;
      $person->middle_name = $old->middle_name;
      $person->last_name = $old->last_name;
      $person->common_name = $old->common_name;
      if($old->title_id === null) {
        $person->title = null;
      } else {
        $person->title = $titles[$old->title_id];
      }
      $person->email = $old->email;
      $person->phone = $old->phone;
      $person->mobile_phone = $old->mobile_phone;
      $person->phone_email = $old->phone_email;
      $person->notes = $old->notes;
      $person->born_on = $old->born_on;
      $person->social_insurance_number = $old->social_insurance_number;
      $person->drivers_license_number = $old->drivers_license_number;
      $person->emergency_contact_id = $old->emergency_contact_id;
      $person->is_canadian_resident = $old->is_canadian_resident;
      $person->license_abstract_permission = $old->license_abstract_permission;
      $person->save();
    }
    return redirect(route('data'));
  }

  public function import_company_people() {
    $old_people = DB::connection('mysql2')->select('SELECT * FROM CompanyPeople');
    $booleans = [
      'false' => false,
      'true' => true
    ];

    foreach($old_people as $old) {
      $person = new CompanyPerson;

      $person->old_id = $old->id;
      $person->id = $old->id;
      $person->custom_id = $old->custom_id;
      $person->company_id = $old->company_id;
      $person->person_id = $old->person_id;
      $person->position = $old->position;
      $person->reports_to = $old->reports_to;
      $person->phone = $old->phone;
      $person->phone1 = $old->phone1;
      $person->email = $old->email;
      $person->status = $old->status;
      $person->hired_on = $old->hired_on;
      if($old->is_payroll === null) {
        $person->is_payroll = null;
      } else {
        $person->is_payroll = $booleans[$old->is_payroll];
      }
      $person->is_salary = $old->is_salary;
      if($old->is_wheniwork === null) {
        $person->is_wheniwork = null;
      } else {
        $person->is_wheniwork = $booleans[$old->is_wheniwork];
      }
      $person->wheniwork_id = $old->wheniwork_id;
      if($old->is_contractor === null) {
        $person->is_contractor = null;
      } else {
        $person->is_contractor = $booleans[$old->is_contractor];
      }
      $person->payweb_emp_id = $old->payweb_emp_id;
      $person->department = $old->department;
      $person->save();
    }
    
    return redirect(route('data'));

  }


  public function import_projects_table() {
    $old_projects = DB::connection('mysql2')->select('SELECT * FROM Projects');

    foreach($old_projects as $old) {
      $project = New Project;
      $project->id = $old->id;
      $project->old_id = $old->id;
      $project->name = $old->name;
      $project->customer_id = $old->customer_id;
      $project->entered_by = $old->entered_by;
      $project->booked_by = $old->booked_by;
      $project->managed_by = $old->managed_by;
      $project->branch_id = $old->branch_id;
      $project->po_number = $old->po_number;
      $project->customer_customer_id = $old->customer_customer_id;
      $project->default_quote_type = $old->default_quote_type;
      $project->archived_on = $old->archived_on;
      $project->changed_on = $old->changed_on;
      $project->updated_at = $old->changed_on;
      $project->save();
    }

    return redirect(route('data'));
  }

  public function import_projectservices_table() {
    $old_project_services = DB::connection('mysql2')->select('SELECT * FROM ProjectServices');

    foreach($old_project_services as $old) {
      // if($old->id < 30000) {
      // if($old->id >= 30000 && $old->id < 60000) {
      // if($old->id >= 60000 && $old->id < 90000) {
      if($old->id >= 90000) {
        $projectService = New ProjectService;
        
        $projectService->id = $old->id;
        $projectService->old_id = $old->id;
        $projectService->project_id = $old->project_id;
        $projectService->project_quote_id = $old->project_quote_id;
        $projectService->service_id = $old->service_id;
        $projectService->branch_id = $old->branch_id;
        $projectService->occurs_on = $old->occurs_on;
        $projectService->occurs_at = $old->occurs_at;
        $projectService->status = $old->status;
        $projectService->invoice_number = $old->invoice_number;
        $projectService->created_by = $old->created_by;
        $projectService->google_calendar_event_id = $old->google_calendar_event_id;
        $projectService->minutes_estimate = $old->minutes_estimate;
        $projectService->minutes_actual = $old->minutes_actual;
        $projectService->archived_on = $old->archived_on;

        $projectService->save();
      }
    }
    return redirect(route('data'));
  }

  public function import_services_table() {
    $old_services = DB::connection('mysql2')->select('SELECT * FROM Services');
    foreach($old_services as $old) {
      $service = New Service;
      $service->id = $old->id;
      $service->old_id = $old->id;
      $service->name = $old->name;
      $service->archived_on = $old->archived_on;
      $service->omn_tenant = $old->omn_tenant_id;

      $service->save();
    }
    return redirect(route('data'));
  }

  public function import_project_service_notes() {
    $old_notes = DB::connection('mysql2')->select('SELECT * FROM ProjectServiceNotes');
    foreach($old_notes as $old) {
      if($old->id >= 55001) {
        $note = New ProjectServicesNotes;
        $note->id = $old->id;
        $note->old_id = $old->id;
        $note->project_service_id = $old->project_service_id;
        $note->entered_by = $old->entered_by;
        $note->entered_on = $old->entered_on;
        $note->content = $old->content;
        $note->access_level = $old->access_level;

        $note->save();
      }
    }
    return redirect(route('data'));
  }

}
