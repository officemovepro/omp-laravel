<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Company;
use App\Project;
use App\ProjectService;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
      $user = \App\User::where('id','=',auth()->user()->id)->first();

        // dd($request->all());
        $project_services = ProjectService::orderBy('occurs_on', 'DESC')
          ->with('project')
          ->with('notes')
          ->paginate(50);

        if($request->project_service_id !== null) {
          $project_services = ProjectService::where('id', '=', $request->project_service_id)
          ->with('project')
          ->orderBy('occurs_on', 'DESC')
          ->paginate(50);
        }
  
        if($request->branch !== null) {
          $project_services = ProjectService::where('branch_id', '=', $request->branch)
          ->with('project')
          ->orderBy('occurs_on', 'DESC')
          ->paginate(50);
        }

      $branches = Company::where('type_id', '=', 2)->orderBy('name')->get();

      // dd($project_services);
      return view('dashboard', ['project_services'=>$project_services, 'branches'=> $branches]);
    }
}
