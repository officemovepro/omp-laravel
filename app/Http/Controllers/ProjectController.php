<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;
use App\ProjectService;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id)
    {
      $project = Project::where('id', '=', $id)
      ->with('project_services')
      ->first();
      dd($project);
      // $project_services = ProjectService::orderBy('occurs_on', 'DESC')
      //   ->with('project')
      //   ->paginate(50);

      // dd($project_services[0]);
      return view('projects.project', ['project'=>$project]);
  }
}
