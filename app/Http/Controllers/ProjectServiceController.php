<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;
use App\ProjectService;

class ProjectServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id)
    {
      $project_service = ProjectService::where('id', '=', $id)
      ->with('project')
      ->with('notes')
      ->first();
      $other_services = ProjectService::where('project_id', '=', $project_service->project_id)->orderBy('occurs_on', 'DESC')->get();
      // dd($other_services);

      return view('projects.project_service', ['project_service'=>$project_service, 'other_services' => $other_services]);
  }
}
