<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
  public function users() {
    return $this->belongsToMany('App\User', 'user_permission');
  }
  
  public function roles() {
    return $this->belongsToMany('App\Role', 'permission_role');
  }
}
