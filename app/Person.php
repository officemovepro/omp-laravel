<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Company;

class Person extends Model
{
    protected $table = 'persons';

    public function company() {
        return $this->hasOne('App\Company', 'id', 'company_id');
    }
}


