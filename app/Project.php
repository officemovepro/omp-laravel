<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function customer() {
        return $this->hasOne('App\Company', 'id','customer_id');
    }
    public function branch() {
        return $this->hasOne('App\Company', 'id','branch_id');
    }
    public function project_services() {
        return $this->hasMany('App\ProjectService');
    }
}
