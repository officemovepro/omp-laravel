<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectService extends Model
{
    public function service() {
        return $this->hasOne('App\Service', 'id','service_id');
    }
    
    public function project() {
        return $this->belongsTo('App\Project');
    }
    
    public function branch() {
        return $this->hasOne('App\Company', 'id', 'branch_id');
    }

    public function notes() {
        return $this->hasMany('App\ProjectServicesNotes');
    }
}
