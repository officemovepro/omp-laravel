<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->integer('old_id');
            $table->integer('type_id');
            $table->string('name');
            $table->string('email');
            $table->string('url');
            $table->string('phone');
            $table->string('fax');
            $table->text('note');
            $table->integer('parent_company_id')->nullable();
            $table->string('primary_colour')->nullable();
            $table->string('secondary_colour')->nullable();
            $table->string('logo_url')->nullable();
            $table->string('url_prefix')->nullable();
            $table->date('verified_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
