<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropFieldsFromPersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('persons', function (Blueprint $table) {
            $table->dropColumn('custom_id');
            $table->dropColumn('company_id');
            $table->dropColumn('position');
            $table->dropColumn('reports_to');
            $table->dropColumn('phone_1');
            $table->dropColumn('status');
            $table->dropColumn('hired_on');
            $table->dropColumn('is_payroll');
            $table->dropColumn('is_salary');
            $table->dropColumn('is_wheniwork');
            $table->dropColumn('wheniwork_id');
            $table->dropColumn('is_contractor');
            $table->dropColumn('payweb_emp_id');
            $table->dropColumn('department');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('persons', function (Blueprint $table) {
            //
        });
    }
}
