<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->integer('old_id');
            $table->string('name')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('entered_by')->nullable();
            $table->integer('booked_by')->nullable();
            $table->integer('managed_by')->nullable();
            $table->integer('branch_id')->nullable();
            $table->string('po_number')->nullable();
            $table->string('customer_customer_id')->nullable();
            $table->enum('default_quote_type',['fixed', 'hourly'])->nullable();
            $table->date('archived_on')->nullable();
            $table->timestamp('changed_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
