<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectServicesNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_services_notes', function (Blueprint $table) {
            $table->id();
            $table->integer('old_id');
            $table->integer('project_service_id')->nullable();
            $table->integer('entered_by')->nullable();
            $table->timestamp('entered_on')->nullable();
            $table->mediumText('content');
            $table->enum('access_level',['private', 'public'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_services_notes');
    }
}
