@extends('layouts.app')

@section('content')
<div class="text-center text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <h2 class="my-8 text-3xl font-extrabold">Services</h2>
  <div class="flex flex-col">
    <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
      <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
        <div class="border mb-4 text-left p-4 flex justify-between">
          <form method="POST" action="{{route('service-filter')}}">
            @csrf
            <div class="flex">
              <input id="project_service_id" type="text" name="project_service_id" class="appearance-none block px-3 py-2 border border-gray-300 rounded-md placeholder-gray-400 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 transition duration-150 ease-in-out sm:text-sm sm:leading-5" placeholder="Project Service ID" />
              <span class="text-lg text-gray-300"> &mdash; or &mdash; </span>
              <select id="branch" name="branch" class="block bg-white border border-gray-300 hover:border-gray-500 px-3 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                <option value="">Select Branch</option>
                @foreach($branches as $branch)
                  <option @if(request('branch') == $branch->id)selected @endif value="{{ $branch->id }}">{{$branch->name}}</option>
                @endforeach
              </select>
              
              <button type="submit" class="flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out ml-4">
                Filter
              </button>
              
              <a href="{{route('dashboard')}}" class="flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out ml-4">
                View All
              </a>
            </div>
          </form>
        </div>
        @foreach($project_services as $project_service)
        <div class="border mb-4 text-left p-4 flex justify-between">
          <div>
            <h2 class="font-semibold">{{$project_service->id}} - {{$project_service->project->customer->name}} - {{$project_service->branch->name}}</h2>
            <p class="h4">{{$project_service->project->name}}</p>
            <p class="h4">{{$project_service->service->name}}</p>
          </div>
          <div class="text-right">
            <p>{{$project_service->occurs_on}}</p>
            <p class="mb-2">{{$project_service->occurs_at}}</p>
            <div>
              <a href="/project_service/{{$project_service->id}}" class="py-1 px-2 border border-transparent text-sm font-small rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out ml-4">View Service</a>
            <a href="/project/{{$project_service->project_id}}" class="py-1 px-2 border border-transparent text-sm font-small rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out ml-2">View Project</a>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
    <div class="flex justify-end mt-3 mb-8">
      @if ($project_services->hasPages())
      {{ $project_services->appends(Request::except('page'))->links() }}
      @endif
    </div>
  </div>
  @endsection