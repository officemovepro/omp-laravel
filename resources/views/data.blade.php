@extends('layouts.app')

@section('content')
<div class="text-center">
  @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
  @endif
  <h2 class=" text-3xl leading-9 font-extrabold text-gray-900 mt-5">Data</h2>
</div>
<div>
  {{-- <form method="POST"  action="{{ route('create_company_types') }}">
    @csrf
    <button type="submit" class="flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out">Create Company Types</button>
  </form>
  <form method="POST"  action="{{ route('import_companies') }}">
    @csrf
    <button type="submit" class="flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out">Import Companies</button>
  </form>
  <form method="POST"  action="{{ route('import_cities') }}">
    @csrf
    <button type="submit" class="flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out">Import Cities</button>
  </form>
  <form method="POST"  action="{{ route('import_countries') }}">
    @csrf
    <button type="submit" class="flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out">Import Countries</button>
  </form>
  <form method="POST"  action="{{ route('import_provinces') }}">
    @csrf
    <button type="submit" class="flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out">Import Provinces</button>
  </form>
  <form method="POST"  action="{{ route('import_addresses') }}">
    @csrf
    <button type="submit" class="flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out">Import Addresses</button>
  </form> 

  <form method="POST"  action="{{ route('import_people') }}">
    @csrf
    <button type="submit" class="flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out">Import People</button>
  </form>

  <form method="POST"  action="{{ route('import_company_people') }}">
    @csrf
    <button type="submit" class="flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out">Import Company People</button>
  </form>
  <form method="POST"  action="{{ route('import_projects_table') }}">
    @csrf
    <button type="submit" class="flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out">Import Projects Table</button>
  </form>

  <form method="POST"  action="{{ route('import_projectservices_table') }}">
    @csrf
    <button type="submit" class="flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out">Import Project Services Table</button>
  </form>
  <form method="POST"  action="{{ route('import_services_table') }}">
    @csrf
    <button type="submit" class="flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out">Import Services Table</button>
  </form>   --}}
  <form method="POST"  action="{{ route('import_project_service_notes') }}">
    @csrf
    <button type="submit" class="flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-600 hover:bg-orange-500 focus:outline-none focus:border-orange-700 focus:shadow-outline-orange active:bg-orange-700 transition duration-150 ease-in-out">Import Project Services Notes</button>
  </form>

</div>
<div>
</div>
@endsection
