@extends('layouts.app')

@section('content')
<div class="text-center text-gray-900 mt-5">
  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif
  <h2 class="my-8 text-3xl font-extrabold">{{$project_service->id}} - {{$project_service->project->customer->name}} -
    {{$project_service->project->name}}</h2>
  <div class="flex flex-col">
    <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
      <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
        <div class="border mb-4 text-left p-4 flex justify-between">
        </div>
      </div>
    </div>
    <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
      <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
        <div class="border mb-4 text-left p-4">
          <h2 class="mb-4 font-bold text-xl">Notes</h2>
          <ul>
            @foreach($project_service->notes as $note)
            <div class="mb-4">
              {{ $note->person->first_name }} {{ $note->person->last_name }} noted on {{$note->entered_on}}: <br>
              {!! $note->content !!} 
              <hr>
            </div>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
    <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
      <div class="align-middle inline-block min-w-full overflow-hidden sm:rounded-lg">
        <div class="border mb-4 text-left p-4">
          <h2 class="mb-4 font-bold text-xl">Other Services</h2>
          <ul>
            @foreach($other_services as $service)
            <li>
              {{$service->id}} - {{$service->service->name}} - {{$service->occurs_on}} - {{$service->occurs_at}} - <a href="/project_service/{{$service->id}}">View</a>
            </li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>
  @endsection