<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
  ]);
  Route::post('/', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
  ]);
// Route::get('/{any}', 'SpaController@index')->where('any', '.*');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::post('/dashboard', 'HomeController@index')->name('service-filter');
Route::get('/project_service/{id}', 'ProjectServiceController@index');
Route::get('/project/{id}', 'ProjectController@index');

// Import data routes
if (App::environment('development'))
{
  Route::get('/data', 'DataController@index')->name('data');
  Route::post('/data/import_companies', 'DataController@import_companies')->name('import_companies');
  Route::post('/data/create_company_types', 'DataController@create_company_types')->name('create_company_types');
  Route::post('/data/import_cities', 'DataController@import_cities')->name('import_cities');
  Route::post('/data/import_countries', 'DataController@import_countries')->name('import_countries');
  Route::post('/data/import_provinces', 'DataController@import_provinces')->name('import_provinces');
  Route::post('/data/import_addresses', 'DataController@import_addresses')->name('import_addresses');
  Route::post('/data/import_people', 'DataController@import_people')->name('import_people');
  Route::post('/data/import_company_people', 'DataController@import_company_people')->name('import_company_people');
  Route::post('/data/import_projects_table', 'DataController@import_projects_table')->name('import_projects_table');
  Route::post('/data/import_projectservices_table', 'DataController@import_projectservices_table')->name('import_projectservices_table');
  Route::post('/data/import_services_table', 'DataController@import_services_table')->name('import_services_table');
  Route::post('/data/import_project_services_notes', 'DataController@import_project_service_notes')->name('import_project_service_notes');
}